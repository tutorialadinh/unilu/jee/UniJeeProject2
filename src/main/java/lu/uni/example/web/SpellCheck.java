package lu.uni.example.web;


import lombok.Getter;
import lombok.Setter;
import lu.uni.example.SpellCheckBeanLocal;
import org.primefaces.context.PrimeRequestContext;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ViewScoped
@ManagedBean(name = "spellClient")
public class SpellCheck {

    @EJB
    SpellCheckBeanLocal sm;

    @Getter
    @Setter
    private String inputTextToCheck;

    @Getter
    @Setter
    private String outputResultRemarkText;


    /**
     * Method executed at startup
     */
    @PostConstruct
    public void init() {
        inputTextToCheck = "";
        outputResultRemarkText = "";
    }

    /**
     * Method called from spellCheck.xhtml when using Key inputs
     * <p>
     * If you've declared 'update' attribute in your xhtml, it will automatically interact with the field defined in the 'value' attribute
     */
    public void handleKeyEvent() {
        // 'inputTextToCheck' will be updated at key inputs (it is declared in the .xhtml.

        /*
         * Define here the list of actions to do.
         */
        // Display Result
        outputResultRemarkText = sm.onSpellCheck(inputTextToCheck);

        boolean hasNoCorrections = false;
        if (outputResultRemarkText.equals("")) {
            outputResultRemarkText = "No errors detected";
            hasNoCorrections = true;
        }

        Integer[] listOfFromIndexes = sm.getListOfFromIndexes();
        Integer[] listOfToIndexes = sm.getListOfToIndexes();

        // Send Callback response to xhtml
        PrimeRequestContext ctx = PrimeRequestContext.getCurrentInstance();
        ctx.getCallbackParams().put("listOfFrom", listOfFromIndexes);
        ctx.getCallbackParams().put("listOfTo", listOfToIndexes);
        ctx.getCallbackParams().put("hasNoCorrections", hasNoCorrections);
    }
}
