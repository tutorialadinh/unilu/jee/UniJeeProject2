package lu.uni.example;

import javax.ejb.Local;

@Local
public interface SpellCheckBeanLocal {
    String onSpellCheck(String inputTextToCheck);
    Integer[] getListOfFromIndexes();
    Integer[] getListOfToIndexes();
}
