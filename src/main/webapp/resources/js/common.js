function highLight(args) {
    var spellTextAreaString = PF('spellTextAreaWidget').jq.val();
    var spellTextAreaStringToDisplay = "";

    // Save the original text from inputTextArea
    var spellTextAreaStringToDisplay = spellTextAreaString;

    var listOfFrom = args.listOfFrom;
    var listOfTo = args.listOfTo;

    for (let i=0; i<listOfFrom.length; i++) {
        // Get Word to replace
        var redText = spellTextAreaString.substring(listOfFrom[i], listOfTo[i]);
//        console.log("TOTO redText : ", redText);

        var offsetString1 = "<a style='background-color: #F08080;'>";
        var offsetString2 = "</a>";

        // Update String
        if(i==0) {
            // At start we didn't add the <a> tag
            spellTextAreaStringToDisplay = spellTextAreaStringToDisplay.substring(0,listOfFrom[i]) + "<a style='background-color: #F08080;'>" + redText + "</a>" + spellTextAreaStringToDisplay.substring(listOfTo[i]);
//            console.log("TOTO spellTextAreaStringToDisplay FIRST TIME: ", spellTextAreaStringToDisplay);
        } else {
            // For the next iterations we have an offset of <a> tag ( * i )
//            console.log("TOTO PREVIOUS spellTextAreaStringToDisplay : ", spellTextAreaStringToDisplay);
//            console.log("TOTO 1 : ", spellTextAreaStringToDisplay.substring(0,listOfFrom[i] + offsetString1.length + offsetString2.length));
//            console.log("TOTO 2 : ", spellTextAreaStringToDisplay.substring(listOfTo[i]  + offsetString1.length + offsetString2.length));
            spellTextAreaStringToDisplay = spellTextAreaStringToDisplay.substring(0,listOfFrom[i] + i * offsetString1.length + i * offsetString2.length) + "<a style='background-color: #F08080;'>" + redText + "</a>" + spellTextAreaStringToDisplay.substring(listOfTo[i]  + i * offsetString1.length + i * offsetString2.length);
//            console.log("TOTO spellTextAreaStringToDisplay : ", spellTextAreaStringToDisplay);
        }
    }

    // Update UI
    var spellTextAreaToHtmlElement = document.getElementById('spellTextAreaToHtml');
    spellTextAreaToHtmlElement.innerHTML = spellTextAreaStringToDisplay;
    spellTextAreaToHtmlElement.style.visibility = "visible";

    if(args.hasNoCorrections == true) {
        spellTextAreaToHtmlElement.style.visibility = "hidden";
    }
}