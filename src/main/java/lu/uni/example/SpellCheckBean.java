package lu.uni.example;

import org.languagetool.JLanguageTool;
import org.languagetool.language.BritishEnglish;
import org.languagetool.rules.RuleMatch;

import javax.ejb.Stateless;
import java.io.IOException;
import java.util.List;

@Stateless
public class SpellCheckBean implements SpellCheckBeanLocal {

    /**
     * List of [match.getFromPos()] to highlight Text
     */
    private Integer[] listOfFromIndexes;

    /**
     * List of [match.getToPos()] to highlight Text
     */
    private Integer[] listOfToIndexes;

    /**
     * Method to check Text spell
     */
    @Override
    public String onSpellCheck(String inputTextToCheck) {
        JLanguageTool langTool = new JLanguageTool(new BritishEnglish());
        // Comment in to use statistical ngram data:
//      langTool.activateLanguageModelRules(new File("/data/google-ngram-data"));

        List<RuleMatch> matches;
        try {
            matches = langTool.check(inputTextToCheck);

            // Init ArrayList
            listOfFromIndexes = new Integer[matches.size()];
            listOfToIndexes = new Integer[matches.size()];

            StringBuilder remark = new StringBuilder();

            for (int i = 0; i < matches.size(); i++) {
                remark.append("Potential error at characters ").append(matches.get(i).getFromPos()).append("-").append(matches.get(i).getToPos()).append("  ->  ").append("suggested correction(s) : ").append(matches.get(i).getSuggestedReplacements()).append("\n").append("\n");
                listOfFromIndexes[i] = matches.get(i).getFromPos();
                listOfToIndexes[i] = matches.get(i).getToPos();
            }

            return remark.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

    @Override
    public Integer[] getListOfFromIndexes() {
        return listOfFromIndexes;
    }

    @Override
    public Integer[] getListOfToIndexes() {
        return listOfToIndexes;
    }
}
